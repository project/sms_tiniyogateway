<?php

namespace Drupal\sms_tiniyogateway\Plugin\SmsGateway;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\sms\Direction;
use Drupal\sms\Entity\SmsGatewayInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessage;
use Drupal\sms\Message\SmsMessageResultStatus;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\SmsProcessingResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @SmsGateway(
 *   id = "tiniyo",
 *   label = @Translation("Tiniyo"),
 *   outgoing_message_max_recipients = -1,
 *   reports_push = TRUE,
 * )
 */
class Tiniyo extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a Tiniyo object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The default HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'tiniyo_auth_id' => '',
      'tiniyo_from' => 'TINIYO',
      'tiniyo_auth_secret_id' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['tiniyo'] = [
      '#type' => 'details',
      '#title' => $this->t('Tiniyo'),
      '#open' => TRUE,
    ];

    $form['tiniyo']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Tiniyo Account Auth ID can be found at <a href="https://www.tiniyo.com/accounts/home">https://www.tiniyo.com/accounts/home</a>.'),
    ];

    $form['tiniyo']['tiniyo_auth_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tiniyo Account Auth ID'),
      '#default_value' => $config['tiniyo_auth_id'],
      '#placeholder' => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
      '#required' => TRUE,
    ];

    $form['tiniyo']['tiniyo_auth_secret_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tiniyo AuthSecretID'),
      '#default_value' => $config['tiniyo_auth_secret_id'],
      '#placeholder' => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
      '#required' => TRUE,
    ];

    $form['tiniyo']['tiniyo_from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From number or Sender ID'),
      '#description' => $this->t('Default is set to "TINIYO". You can create new SenderID at <a href="https://www.tiniyo.com" target="_blank">https://www.tiniyo.com</a> and add here.'),
      '#default_value' => $config['tiniyo_from'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['tiniyo_auth_id'] = trim($form_state->getValue('tiniyo_auth_id'));
    $this->configuration['tiniyo_auth_secret_id'] = trim($form_state->getValue('tiniyo_auth_secret_id'));
    $this->configuration['tiniyo_from'] = $form_state->getValue('tiniyo_from');
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message) {
    $recipient = $sms_message->getRecipients()[0];
    $message = $sms_message->getMessage();

    $base_uri = 'https://api.tiniyo.com/v1/';

    $result = new SmsMessageResult();

    $report = (new SmsDeliveryReport())
      ->setRecipient($recipient);

    $tiniyo_auth_id = $this->configuration['tiniyo_auth_id'];
    $tiniyo_auth_secret_id = $this->configuration['tiniyo_auth_secret_id'];
    $tiniyo_from = $this->configuration['tiniyo_from'];
    $settings = [
      'auth' => [$tiniyo_auth_id, $tiniyo_auth_secret_id],
      'headers' => [
        'Content-Type' => 'application/json',
      ],
      'json' => [
        'src' => $tiniyo_from,
        'dst' => strval($recipient),
        'method' => 'post',
        'text' => $message,
      ]
    ];

    $url = $base_uri . 'Account/' . $tiniyo_auth_id . '/Message';
    try {
      $response = $this->httpClient
        ->request('post', $url, $settings);
    }
    catch (RequestException $exception) {
      $response = $exception->getResponse();
      \Drupal::logger('sms_tiniyo')->error($exception->getMessage());
    }

    $http_code = $response->getStatusCode();
    if ($http_code == 200) {
      $report->setStatus(SmsMessageReportStatus::QUEUED);
      $report->setTimeQueued(REQUEST_TIME);
      \Drupal::logger('sms_tiniyo')->notice('sms sent to %number', ['%number' => strval($recipient)]);
    }

    if ($report->getStatus()) {
      $result->setReports([$report]);
    }

    return $result;
  }

}
